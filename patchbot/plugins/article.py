from __future__ import annotations

import datetime
import json
import re
from dataclasses import dataclass, field
from typing import Any, Sequence

import hikari
import tanjun
import yuyo

component = tanjun.Component()

SITE_NAME = "https://patchwork.systems"
CATEGORY_URL = f"{SITE_NAME}/category/{{0}}.html"
TAGS_RE = "tanjun|hikari|python"


@dataclass(init=True, repr=True)
class ArticleEntry:
    title: str
    body: str
    url: str
    category: str
    tags: set[str] = field(default_factory=set)

    def full_url(self):
        return f"{SITE_NAME}/{self.url}"

    def category_url(self):
        return f"{SITE_NAME}/category/{self.category}.html"

    def __hash__(self):
        return hash(f"{self.title}--{self.url}")


SORTED_ARTICLES: dict[str, dict[str, ArticleEntry]] = dict()


with open("tipuesearch_content.js") as site_index_handler:
    site_data = site_index_handler.read()[17:-1]
    site_dict = json.loads(site_data)
    for page in site_dict.get("pages"):
        if not SORTED_ARTICLES.get(page["tags"], False):
            SORTED_ARTICLES[page["tags"]] = dict()
        SORTED_ARTICLES[page["tags"]][page["title"]] = ArticleEntry(
            title=page["title"],
            body=page["text"],
            url=page["url"],
            category=page["tags"],
            tags=set(re.findall(TAGS_RE, page["text"], flags=re.IGNORECASE)),
        )


CATEGORY_CHOICES = list(SORTED_ARTICLES.keys())

article = component.with_slash_command(
    tanjun.slash_command_group("article", "Work with articles from Patchwork.systems")
)


def chunker(in_seq: Sequence[Any], size: int) -> list[Sequence[Any]]:
    seq = list(in_seq)
    return [seq[pos : pos + size] for pos in range(0, len(seq), size)]


def _gen_embed(articles: Sequence[ArticleEntry], title: str | None = None, url: str | None = None) -> hikari.Embed:
    embed = hikari.Embed(title=title, url=url)
    embed.set_thumbnail("https://cdn.discordapp.com/attachments/838613230536359997/884605473167384626/logo128.png")
    for article in articles:
        body = f"{article.body[:200]}...\n [Learn more>>]({article.full_url()})"
        embed.add_field(name=article.title.title(), value=body)
    return embed


@article.with_command
@tanjun.with_str_slash_option("category", "The category to view.", choices=CATEGORY_CHOICES)
@tanjun.as_slash_command("list_category", "List all articles in a category.")
async def list_category(
    ctx: tanjun.abc.SlashContext,
    category: str,
    component_client: yuyo.ComponentClient = tanjun.inject(type=yuyo.ComponentClient),
):
    embed = hikari.Embed()
    category_article_sets = chunker(list(SORTED_ARTICLES.get(category, {}).values()), 25)
    iterator = (
        (
            hikari.UNDEFINED,
            _gen_embed(category_article, title=f"Index of {category.title()}", url=CATEGORY_URL.format(category)),
        )
        for category_article in category_article_sets
    )
    paginator = yuyo.ComponentPaginator(
        iterator,
        authors=(ctx.author,),
        triggers=(
            yuyo.pagination.LEFT_DOUBLE_TRIANGLE,
            yuyo.pagination.LEFT_TRIANGLE,
            yuyo.pagination.STOP_SQUARE,
            yuyo.pagination.RIGHT_TRIANGLE,
            yuyo.pagination.RIGHT_DOUBLE_TRIANGLE,
        ),
        timeout=datetime.timedelta(days=99999),
    )
    components = paginator.builder()
    if first_response := await paginator.get_next_entry():
        content, embed = first_response
        message = await ctx.respond(content=content, components=components, embed=embed, ensure_result=True)
        component_client.set_executor(message, paginator)
        return

    await ctx.respond(embed)


@article.with_command
@tanjun.with_str_slash_option("terms", "A term to lookup")
@tanjun.with_str_slash_option("category", "What kind of article to search", choices=CATEGORY_CHOICES)
@tanjun.as_slash_command("search", "Search an article on patchwork.systems")
async def article_search(
    ctx: tanjun.abc.SlashContext,
    category: str,
    terms: str,
    component_client: yuyo.ComponentClient = tanjun.inject(type=yuyo.ComponentClient),
):
    terms_list = terms.lower().split(" ")
    category_articles = list(SORTED_ARTICLES.get(category, {}).values())
    if not category_articles:
        await ctx.respond("No articles found!")
        return
    term_articles: set[ArticleEntry] = set()

    for category_article in category_articles:
        for term in terms_list:
            if (
                term in category_article.title.lower()
                or term in category_article.tags
                or term in category_article.body.lower()
            ):
                term_articles.add(category_article)

    term_articles_set = chunker(term_articles, 25)
    for k, aset in enumerate(term_articles_set):
        print(f"Set {k}")
        for article in aset:
            print("*" * 80)
            print(article.title)

    iterator = (
        (
            hikari.UNDEFINED,
            _gen_embed(
                article,
                title=f"Searching Patchwork.Systems for {terms}",
                url=f"{SITE_NAME}/search.html?q={terms.replace(' ', '%20')}",
            ),
        )
        for article in term_articles_set
    )
    paginator = yuyo.ComponentPaginator(
        iterator,
        authors=(ctx.author,),
        triggers=(
            yuyo.pagination.LEFT_DOUBLE_TRIANGLE,
            yuyo.pagination.LEFT_TRIANGLE,
            yuyo.pagination.STOP_SQUARE,
            yuyo.pagination.RIGHT_TRIANGLE,
            yuyo.pagination.RIGHT_DOUBLE_TRIANGLE,
        ),
        timeout=datetime.timedelta(days=99999),
    )
    components = paginator.builder()
    if first_response := await paginator.get_next_entry():
        content, embed = first_response
        message = await ctx.respond(content=content, components=components, embed=embed, ensure_result=True)
        component_client.set_executor(message, paginator)
        return
    else:
        embed = hikari.Embed(title="No Results")
        await ctx.respond(embed)


loaders = component.make_loader()
