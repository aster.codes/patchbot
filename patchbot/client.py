"""Module provides the basic functions to build the client and bot."""
from __future__ import annotations

import typing

import config as config_
import hikari
import tanjun
import yuyo

# from util.dependencies import SITE_DATA
from hikari import config as hikari_config
from util.hooks import on_error, on_parse_error


def build_bot() -> hikari.GatewayBot:
    """Builds and returns a GatewayBot. Calls build_client to start tanjun."""
    config = config_.FullConfig.from_environ()
    bot = hikari.GatewayBot(
        config.tokens.discord_bot,
        logs=config.log_level,
        intents=config.intents,
        cache_settings=hikari_config.CacheSettings(components=config.cache),
        #        banner="util",
    )

    build_client(bot, config=config)

    return bot


def build_client(bot: hikari.GatewayBot, /, *, config: config_.FullConfig | None = None) -> tanjun.Client:
    """Builds and configures the tanjun client from a given GatewayBot."""
    if not config:
        config = config_.FullConfig.from_environ()

    client = tanjun.Client.from_gateway_bot(
        bot, mention_prefix=config.mention_prefix, declare_global_commands=config.set_global_commands
    )

    client.set_hooks(tanjun.AnyHooks().set_on_parser_error(on_parse_error).set_on_error(on_error))
    component_client = yuyo.ComponentClient.from_gateway_bot(bot, event_managed=False)

    client = (
        client.add_prefix(config.prefixes)
        .add_client_callback(tanjun.ClientCallbackNames.STARTING, component_client.open)
        .add_client_callback(tanjun.ClientCallbackNames.CLOSING, component_client.close)
        .set_type_dependency(yuyo.ComponentClient, component_client)
        .set_type_dependency(config_.FullConfig, lambda: typing.cast(config_.FullConfig, config))
        .set_type_dependency(config_.Tokens, lambda: typing.cast(config_.FullConfig, config).tokens)
        #        .set_type_dependency(SITE_DATA, sorted_dict)
        .load_modules("plugins.article")
    )

    if config.owner_only:
        client.set_check(tanjun.checks.ApplicationOwnerCheck())

    return client
