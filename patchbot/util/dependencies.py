"""Dependency Injectors for Patchbot"""
from __future__ import annotations

import logging
import typing

import httpx

SITE_DATA = typing.NewType("SiteData", object)


class HttpxInjector:
    """
    Dependency injector for httpx with tanjun.

    Register dependency:
        client.set_type_dependency(httpx.AsyncClient, HttpxInjector())

    Then get in plugin:

    async def func_name(redis: httpx.AsyncClient = tanjun.injected(type=httpx.AsyncClient))
    """

    def __call__(self) -> httpx.AsyncClient:
        logging.info("New httpx Client Spawned")
        return httpx.AsyncClient()
