<div align="center">

# PatchBot

Site Indexer for Patchwork.systems

</div>

This bot provides a simple Discord command interface to get links and articles for Patchwork.systems.

## Commands

`/article list_category category:<str>`

**Accepts:**

`category` - required - The name of a category on the website. Choices a pre-supplied.

Lists all articles in a given category.

`/article search category:<str> terms:<str>`

**Accepts:**

`category` - required - The name of a category on the website. Choices a pre-supplied.

`terms` - required - Terms to search the category for. Multiple terms can be supplied with spaces.

Searches a provided category for a term or list of terms. Headlines, body text and categories are considered in this search.
